#ifndef __EC600S_H_
#define __EC600S_H_

#include "main.h"
#include "usart.h"
#include "stdio.h"
#include "string.h"
#include <ctype.h>
#include <stdlib.h>
#include "gpio.h"
#include "cmsis_os.h"
#include "task.h"
#include "cJSON.h"

#define Normal			0
#define ExecFail		1
#define SendTimeOut		2
#define ReceTimeOut		3
#define ParaErr			4
#define Overflow		5
#define OtherErr			6
#define Queue_Size      1200   //一定要大于等于  消息队列创建时的大小

typedef struct
{
    uint8_t  CSQ;    //信号值
    uint16_t BIN_len; //BIN文件总长度
	  uint8_t  Server_version; //云端版本
	  uint8_t  Board_version; //本地版本
    uint8_t  IMEI[20];  //设备IMEI
} cat_1_type;
extern cat_1_type EC600S_type;  

extern uint8_t AT_send[200]; 
uint8_t EC600S_Wait_Wakeup(void);
uint8_t EC600S_ATE0_EN(void); //关闭回显
uint8_t EC600S_WAIT_CREG(void); //等待设备驻网
uint8_t EC600S_GET_CSQ(void); //获取当前信号值
uint8_t EC600S_GET_IMEI(void); //获取IMEI身份标识
uint8_t EC600S_GET_Version(uint8_t *Server_address); //获取云端版本
uint8_t EC600S_Get_Bin(uint8_t *Server_address); //获取云端文件
uint8_t EC600S_Check_Net(void);
uint8_t EC600S_Connect_MQTT(uint8_t *Server_address,uint16_t Server_port,uint8_t *clientid,uint8_t *username,uint8_t *password);
uint8_t EC600S_Subscribe_MQTT(uint8_t * Sub_Topic,uint8_t qos);
uint8_t EC600S_Publish_MQTT(uint8_t *Publish_topic,uint8_t *Message,uint8_t Csq);
uint8_t EC600S_AT_SR( char *pcCmd2Send, char *ppcSucBuf,  char *ppcErrBuf, TickType_t xTicksToWait);
#endif



