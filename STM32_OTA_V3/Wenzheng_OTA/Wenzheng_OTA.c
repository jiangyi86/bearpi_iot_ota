/**
 * @Copyright 			(c) 2020,wenzheng All rights reserved
 * @filename  			 Wenzheng_OTA.c
 * @breif				     STM32L431 Bootloader
 * @version
 *            			    v1.0    完成程序跳转                          wenzheng    2020/11/17
 *                      v1.1    完成文件搬运和烧录                     wenzheng    2020/11/18
 *                      v2.0    完成外部Flash存储与固件回滚            wenzheng    2020/11/25
 * @note                移植说明（非常重要）：
 *                      1. 本资料参考自CSDN胡工的bootloader框架，并做了L431的适配性修改
 *                         主要差异在于L431的Flash操作方式不同，也是BootLoader编写的重要知识点
 *                         https://blog.csdn.net/weixin_41294615/article/details/104669663
 *                      2. 转载请注明出处
 */

#include "Wenzheng_OTA.h"
/* 标记升级完成 */
void Set_Update_Down(void)
{
    uint8_t update_flag = 0xAA;				///< 对应bootloader的启动步骤
    WriteFlash((Application_2_Addr + Application_Size - 8), &update_flag,1 );
}
/**
 * @bieaf 获取当前页
 *  L431每页2K  共128页  256K
 * @param Addr  当前地址，尽量以页的起始地址
 * @param
 * @return 1
 */
static uint32_t GetPage(uint32_t Addr)
{
    uint32_t page = 0;

    if (Addr < (FLASH_BASE + FLASH_BANK_SIZE))
    {
        /* Bank 1 */
        page = (Addr - FLASH_BASE) / FLASH_PAGE_SIZE;
        printf("Bank 1 \r\n");
    }
    else
    {
        /* Bank 2 */
        page = (Addr - (FLASH_BASE + FLASH_BANK_SIZE)) / FLASH_PAGE_SIZE;
        printf("Bank 2 \r\n");
    }

    return page;
}

/**
 * @bieaf 擦除页
 *  L431每页2K  共128页  256K
 * @param pageaddr  起始地址
 * @param num       擦除的页数
 * @return 1
 */
int Erase_page(uint32_t pageaddr, uint32_t num)
{
    uint32_t PageError = 0;
    HAL_StatusTypeDef status;
    __disable_irq();
    //清除标志位，经测试 必要！！！
    __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_SIZERR | FLASH_FLAG_OPTVERR | FLASH_FLAG_PGSERR | FLASH_FLAG_PROGERR | FLASH_FLAG_BSY);

    HAL_FLASH_Unlock();
    /* 擦除FLASH*/
    FLASH_EraseInitTypeDef FlashSet;
    FlashSet.Banks   = FLASH_BANK_1;      //L431 只有Bank1
    FlashSet.TypeErase = FLASH_TYPEERASE_PAGES;//按页擦除
    FlashSet.Page  = GetPage(pageaddr);//获取页位置
    FlashSet.NbPages = num;  //擦除的页数
    printf("识别的初始页数：%d  删除%d 页\r\n",FlashSet.Page,num);
    /*设置PageError，调用擦除函数*/
    status = HAL_FLASHEx_Erase(&FlashSet, &PageError);
    if (status != HAL_OK)
    {
        printf("HAL_FLASHEx_Erase  ERROR\r\n");
    }
    printf("errcode = %x\n", pFlash.ErrorCode);
    HAL_FLASH_Lock();
    __enable_irq();
    return 1;
}


/**
 * @bieaf 写若干个数据
 *  L4 只能双字节写入
 * @param addr       写入的地址
 * @param buff       写入的数据指针  已更改为8位
 * @param buf_len    buff长度
 * @return
 */
void WriteFlash(uint32_t addr, uint8_t * buff, int buf_len)
{
    /* 1/4解锁FLASH*/
    HAL_FLASH_Unlock();

    for(int i = 0; i < buf_len; i+=8)
    {
        /* 3/4对FLASH烧写*/
        HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, addr+i, *(uint64_t*)(buff+i));
    }
    /* 4/4锁住FLASH*/
    HAL_FLASH_Lock();
}

/**
 * @bieaf 读若干个数据
 * @param addr       读数据的地址
 * @param buff       读出数据的数组指针  已更改为8位
 * @param buf_len    读取的数据长度
 * @return
 */
void ReadFlash(uint32_t dest_addr, uint8_t * buff, int buf_len)
{
    uint32_t i;
    for(i = 0; i < buf_len; i++)
    {
        buff[i] = *(__IO uint8_t*)(dest_addr + i);
    }
    return;
}



/* 读取启动模式 */
unsigned int Read_Start_Mode(void)
{
    uint8_t mode = 0;
    ReadFlash((Application_2_Addr + Application_Size - 8), &mode, 1);
    return mode;
}
/**
 * @bieaf 进行程序的覆盖
 * @detail 1.擦除目的地址
 *         2.源地址的代码拷贝到目的地址
 *         3.擦除源地址
 * @param  搬运的源地址
 * @param  搬运的目的地址
 * @return 搬运的程序大小
 */
void MoveCode(unsigned int src_addr, unsigned int des_addr, unsigned int byte_size)
{
    /*1.擦除目的地址*/
    printf("> Start erase des flash......\r\n");
    Erase_page(des_addr, (byte_size/PageSize));
    printf("> Erase des flash down......\r\n");

    /*2.开始拷贝*/
    uint8_t temp[1024];

    printf("> Start copy......\r\n");
    for(int i = 0; i < byte_size/1024; i++)
    {
        ReadFlash((src_addr + i*1024), temp, 1024);
        WriteFlash((des_addr + i*1024), temp, 1024);
    }
    printf("> Copy down......\r\n");

    /*3.擦除源地址*/
    printf("> Start erase src flash......\r\n");
    Erase_page(src_addr, (byte_size/PageSize));
    printf("> Erase src flash down......\r\n");
}


/* 采用汇编设置栈的值 */
__asm void MSR_MSP (uint32_t ulAddr)
{
    MSR MSP, r0 			                   //set Main Stack value
    BX r14
}


/* 程序跳转函数 */
typedef void (*Jump_Fun)(void);
void IAP_ExecuteApp (uint32_t App_Addr)
{
    Jump_Fun JumpToApp;

    if ( ( ( * ( __IO uint32_t * ) App_Addr ) & 0x2FFE0000 ) == 0x20000000 )	//检查栈顶地址是否合法.
    {
        JumpToApp = (Jump_Fun) * ( __IO uint32_t *)(App_Addr + 4);				//用户代码区第二个字为程序开始地址(复位地址)
        MSR_MSP( * ( __IO uint32_t * ) App_Addr );								//初始化APP堆栈指针(用户代码区的第一个字用于存放栈顶地址)
        JumpToApp();															                //跳转到APP.
    } else {
        printf("JumpToApp--ERROR!!!\r\n");
    }
}


/**
 * @bieaf 进行BootLoader的启动
 * @param none
 * @return none
 */
void Start_BootLoader(void)
{
    /*==========打印消息==========*/
    printf("\r\n");
    printf("***********************************\r\n");
    printf("*                                 *\r\n");
    printf("*    Wenzheng's BootLoader        *\r\n");
    printf("*                                 *\r\n");
    printf("***********************************\r\n");

    printf("> Choose a startup method......\r\n");
    switch(Read_Start_Mode())									///< 读取是否启动应用程序 */
    {
    case Startup_Normol:										///< 正常启动 */
    {
        printf("> Normal start......\r\n");
        break;
    }
    case Startup_Update:										///< 升级再启动 */
    {
        printf("> Start update......\r\n");
        MoveCode(Application_2_Addr, Application_1_Addr, Application_Size);
        printf("> Update down......\r\n");
        break;
    }
    case Startup_Reset:										///<   目前没使用 */
    {
        printf("> Restore to factory program......\r\n");
        break;
    }
    default:														///< 启动失败
    {
        printf("> Error:%X!!!......\r\n", Read_Start_Mode());
        return;
    }
    }

    /* 跳转到应用程序 */
    __disable_irq() ;  //很重要！经测试STM32F4必要！  貌似F105也需要   L431 裸机却不需要  但是FREERTOS需要
    printf("> Start up......\r\n\r\n");
    IAP_ExecuteApp(Application_1_Addr);
}

